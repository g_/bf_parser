from discord.ext.commands import Bot
import settings

possible_commands = ['.', '[', ']', '+', '-', '<', '>']
possible_bits = [8,16]
bits = 8
max_int = pow(2, bits)-1

def isOdd(n):
    return n%2 == 0

def strip(input):
	return [x for x in input if x in possible_commands]

def bf_interpret(string):
	commands = list(string)
	commands = strip(commands, possible_commands)
	array = [0]*256
	loops = []
	returnList = []
	index = 0
	command_index = 0
	while command_index < len(commands):
		command = commands[command_index]
		if (command == "+"):
			array[index] += 1
			# 8 bit positive integer wrapping
			if (array[index] > max_int):
				array[index] = 0
		elif (command == "-"):
			array[index] -= 1
			# 8 bit positive integer wrapping
			if (array[index] < 0):
				array[index] = max_int
		elif (command == ">"):
			index += 1
			# If we reach the end of the array, append another element
			if (index >= len(array)):
				array.append(0)
		elif (command == "<"):
			index -= 1
		elif (command == "["):
			loops.append(command_index+1)
		elif (command == "]"):
			# If our current index value is 0, delete the current loop and continue
			# Else change our command index to whatever was stored in loops
			if (array[index] == 0):
				del loops[len(loops)-1]
			else:
				command_index = loops[len(loops)-1]-1
		elif (command == "."):
			# Add the character to the string return array
			returnList.append(chr(array[index]))
		command_index += 1
	return "".join(returnList)

def bf_generate(string):
	returnString = ""
	bf_state = [0, 0]
	for character in string:
		number = ord(character) - bf_state[1]
		if (number > 0):
			factor = middle(factors(number))
			for x in range(factor[0]):
				returnString += "+"
			returnString += "[>"
			for x in range(factor[1]):
				returnString += "+"
			returnString += "<-]>.<"
		elif (number < 0):
			factor = middle(factors(abs(number)))
			for x in range(factor[0]):
				returnString += "+"
			returnString += "[>"
			for x in range(factor[1]):
				returnString += "-"
			returnString += "<-]>.<"
		else:
			returnString += ">.<"
		bf_state[1] = ord(character)
	return returnString

def middle(list):
	if (len(list)%2 == 0):
		list.sort()
		size = len(list)
		first = int(size/2) - 1
		second = int(size/2)
		return [list[first],list[second]]
	else:
		raise ValueError("List must been of even size")

def factors(n):
	#Don't ask, just do
    return list(x for tup in ([i, n//i] for i in range(1, int(n**0.5)+1) if n % i == 0) for x in tup)

bf = Bot(command_prefix="/")

@bf.command(help="Executes the brainfuck code")
async def execute(*, args):
	await bf.say(bf_interpret(args))

@bf.command(help="Generates brainfuck code from given string")
async def generate(*, args):
	await bf.say(bf_generate(args))

@bf.command(help="Sets bit size for integers (Useful for under and overflow)")
async def setbits(*args):
	try:
		global bits
		global max_int

		num = int(args[0])
		if num in possible_bits:
			bits = num
			max_int = pow(2, num)-1
			await bf.say("Now using {} bit".format(num))
		else:
			await bf.say("Can only use {}".format(possible_bits))
	except Exception as e:
		print(e, e.with_traceback())

if __name__ == "__main__":
	 print("https://discordapp.com/api/oauth2/authorize?client_id={}&scope=bot&permissions=0".format(settings.DISCORD_CLIENT))
	 bf.run(settings.DISCORD_BOT_TOKEN)